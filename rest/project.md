## DRF project

Build CRUD APIs for your entities

Once again, let's take StackOverflow as an example. Here's how CRUD APIs look for `questions` 

_____

### 1. Question List - `/questions`

#### GET: 

When a GET request is made to `/questions`, the server responds with a list of questions in the JSON format.

```json
{
  "items": [
    {
      "id": 1,
      "title": "How to exit vim",
      "body": "..."
    },
    {
      "id": 2,
      "title": "How to configure logging in Python",
      "body": "..."
    }
  ]
}
```

In real world application, you can't just list all the questions in your database. So usually you also apply filters using query parameters: 

Examples: 

Pagination: `/questions?page=1&page_size=10&order_by=id` - All questions in page 1 ordered by id

Filter by user: `/questions?user_id=1` 

And so on...

When you've built a REST API, the client(browser, mobile app etc) can customize the data that is rendered on the client. So we've decoupled the client-rendering logic from the server-side templating code. The client is now fully in control of how the data is displayed. 

#### POST: To create a question, the client makes a POST request to the **same** url, `/questions`, 

So the client will send some data as part of the request:

```json
{
    "title": "Iterating over datetime objects?",
    "body": "..."
}
```

The server will now save the data and respond with the new question:

```json
{
    "id": 4,
    "title": "Iterating over datetime objects?",
    "body": "..."
}
```

Notice how the server sent back the `question_id`. The client can now use this id to perform more operations on this question


### Question Detail - `/questions/<question_id`

#### GET

To see all the info about a particular question, the client can now make a request to `/questions/<question_id`. The server will respond with a JSON object


```json
    {
      "id": 1,
      "title": "How to exit vim",
      "body": "...",
      "answers": [
          {
              "id": 10,
              "body": "..."
          }
      ]
    }
```

Notice that the server can also provide information about related entities like the answers belonging to the question. 

#### PUT

Clients can also update questions by making a PUT request to the Question Detail url.

```json
{
    "id": 4,
    "title": "Updated: Iterating over datetime objects?",
    "body": "..."
}
```

The server will know which question needs to be updated based on the URL. 

#### DELETE

Clients can delete questions by making a DELETE HTTP request to the Question Detail URL

__________


That's how you build a CRUD API. 

Notice how we used the same URLs for different operations, and relied on the type of the HTTP method to perform various actions. 

_________

Go ahead and build CRUD API for your own entities: 

