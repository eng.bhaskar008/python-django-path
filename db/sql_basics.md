## SQL basics

Complete the [W3 schools tutorial](https://www.w3schools.com/sql/)

If you are already familiar with SQL, focus on the topics you are not comfortable with yet.

## Drills

https://pgexercises.com


## Data Project using Python and SQL

## Step 1 - Queries

Figure out the SQL query for each question. Save these these queries in a text file. 

1. Run the queries with the mock data - Verify your result
2. Run queries with real data

## Step 2 - Connect to the database using Python

Create a module called as `sql_queries.py` and define separate functions for each query. These functions **must** return the same data as your original business logic functions written using Python.

Note: You will have to transform the data returned by `psycopg2` into a form which can be passed as an argument to the plot function.

## Step 3 - Plot the data

Import the appropriate function from `sql_queries.py`, and define another function which first calls this function and then passes its result to the plot function.


__________________

