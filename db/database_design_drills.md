# Design normalized relational database schmemas for the following applications

### Workflow:

1. Create Django models instead of writing plain SQL
2. Run migrations and ensure there are no errors
3. Create a few sample rows from the Django shell and check if the design feels OK. If not make changes and repeat the process.

### Common checklist:

* Follow standard Django naming conventions for both models and fields
* Focus on the most important fields and relationships between models. Don't waste time on adding unnecessary fields
* Apply appropriate constraints (ForeignKey, unique_together etc)
______

## Reddit:

If you're not a Redditor, visit https://old.reddit.com and understand the various features of the site.

Design a suitable schema to support the following features. 


1. A user can signup and login with a password and username
2. The site has various subreddits.
3. A user can post links to a specific subreddit
4. Posts can be upvoted and downvoted by other users. Each user can vote on a post once.
5. Users can comment on posts. A comment can either be top-level comment or a reply to another comment.


____________________________


## Gitlab

(Multi-tenant SaaS example)

1. A user can signup and login with a password and email
2. Users can add multiple SSH keys to their account
3. The application needs to support multiple organizations. 
4. Each organization will have multiple admins
5. Admins can add users to their organization
6. An organization will have many projects.
7. Each project will have one git repository associated with it.
8. Each project will also have many issues associated with it. For example, see (https://github.com/twbs/bootstrap/issues)
9. An issue can be opened by an user belonging to the organization. The issue will have a title and a description.
10. Other users in the organization can comment on the issue.
11. An issue can either be in OPEN OR CLOSED state. 
12. An issue can have multiple labels associated with it.

_________________________________



