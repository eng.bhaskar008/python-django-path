1. Create the following directory structure:

```
├── git_workshop
│   ├── a.txt
│   ├── b.txt
│   └── one
│       ├── c.txt
│       └── two
│           ├── d.txt
│           └── three
```

2. Add some arbitrary content to each file.
3. Initialize a git repo at `git_workshop`
4. Add `a.txt` and `b.txt` to the staging area
5. Run git status. Understand what it says.
6. Commit `a.txt` and `b.txt` with the message "Add a.txt and b.txt"
7. Run git status. Understand what it says
8. Run git log
9. Add and Commit `c.txt` and `d.txt`
10. Create a project on GitLab.
11. Push your code to GitLab
12. git clone your project in another directory called `git_workshop_2`
13. In `git_workshop_2`, add a file called `e.txt`
14. Commit and push `e.txt`
15. cd back to `git_workshop`
16. Pull the changes from GitLab
17. Remove `a.txt` from git
18. Run `git log --oneline --decorate --graph`
19. Create an SSH key and add it to GitLab

## Check your understanding

* What happens when you run `git init`?
* Can you run git commands from any directory? 
* At a high level, what happens when you run `git commit`?
* How do you go back in history?
* You added a bunch of commits and pushed the changes to GitLab. Unfortunately, a bug was introduced. How do you undo the changes?
